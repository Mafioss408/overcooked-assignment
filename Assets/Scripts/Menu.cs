using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;



[System.Serializable]
public class Menu
{
   
    public string nameDish;
    public string ingredient_1;
    public string ingredient_2;

    public Menu(string _nameDish,string _ingredient_1, string _ingredient_2) 
    {
        nameDish = _nameDish;
        ingredient_1 = _ingredient_1;
        ingredient_2 = _ingredient_2;
    }

}
