using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[System.Serializable]
public class MyResource
{
    public string namePosition;
    public Transform positionTransform;
    public int numRisorse;

    public MyResource(string namePosition, Transform positionTransform, int numRisorse)
    {
        this.namePosition = namePosition;
        this.positionTransform = positionTransform;
        this.numRisorse = numRisorse;

    }
}
