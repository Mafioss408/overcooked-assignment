using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class UI
{

    public delegate void OnUI();
    public static OnUI onUI;

}
public class PlayerDelegate
{
    public delegate bool OnPlayer();
    public static OnPlayer onPlayer;
    public static OnPlayer onPlayerSleep;
}


public class GameManager : MonoBehaviour
{
    private static GameManager instance;

    public static GameManager Instance
    {
        get => instance;

        private set => instance = value;
    }

    [Header("TextUI")]
    [SerializeField] private TextMeshProUGUI roundText;
    [Header("Round")]
    [SerializeField] private int round;

    [SerializeField] private List<TextMeshProUGUI> optionMenu = new List<TextMeshProUGUI>();

    private ResourceManager resource;


    public int Round { get => round; set => round = value; }

    private void Awake()
    {
        if (instance != null)
        {
            Destroy(gameObject);
        }
        else
        {
            instance = this;
        }
        
    }

    private void Start()
    {
        resource = ResourceManager.Instance;
        UI.onUI?.Invoke();
    }
    private void Update()
    {

        roundText.text = "Round: " + round;
    }

    private void SetOptionMenu()
    {
        for (int i = 0; i < optionMenu.Count; i++)
        {
            optionMenu[i].text = resource.MyResources[i].namePosition + ": " + resource.MyResources[i].numRisorse;
        }
    }

    // zone button
    public void OpenMenu(GameObject obj)
    {
        obj.SetActive(true);
    }
    public void CloseMenu(GameObject obj)
    {
        obj.SetActive(false);
    }
    public void Leave()
    {
        Application.Quit();
    }
    private void OnEnable()
    {
        UI.onUI += SetOptionMenu;
    }

    private void OnDisable()
    {

        UI.onUI -= SetOptionMenu;

    }

}
