using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class TimerManager : MonoBehaviour
{
    private static TimerManager instance;

    public static TimerManager Instance
    {
        get => instance;

        private set => instance = value;
    }


    private void Awake()
    {
        if (instance != null)
        {
            Destroy(gameObject);
        }
        else
        {
            instance = this;
        }

    }

    private float timer;
    private bool normalTimer = true;

    [SerializeField] private float multiplyTimer;
    [SerializeField] private TextMeshProUGUI timeText;


    public bool NormalTimer { get { return normalTimer; } set {  normalTimer = value; } }
    public float Timer { get { return timer; } }

    void Update()
    {
        if(normalTimer)
            NormalMyTimer();
        else
            MultiplyMyTimer();
    
 
        DisplayTime(timer);

    }
    void DisplayTime(float timeToDisplay)
    {
       
        float minutes = Mathf.FloorToInt(timeToDisplay / 60);
        float seconds = Mathf.FloorToInt(timeToDisplay % 60);
        timeText.text = string.Format("{0:00}:{1:00}", minutes, seconds);

        if (timer >= 1440)
        {
            timer = 0;
        }
    }

    private void MultiplyMyTimer()
    {
        timer += Time.deltaTime * multiplyTimer;

    }

    private void NormalMyTimer()
    {
        timer += Time.deltaTime;

    }
}
