using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Player : MonoBehaviour
{
    [Header("Stats")]
    [SerializeField] private float speed;
    [SerializeField] private float speedRotation;
    [Header("Stats Dalay")]
    [SerializeField] private float dalayGoInside;
    [SerializeField] private float dalayGoOutside;
    [Header("MyTurn")]
    [SerializeField] private float wakeUp;
    [SerializeField] private float startTurn;
    [SerializeField] private float eveningShift;

    [Header("Reference")]
    [SerializeField] private Transform badPlayer;
    [SerializeField] private GameObject plate;
    

    [SerializeField] private List<float> convert = new List<float> ();

    private NavMeshAgent nav;
    private ResourceManager resource;

    private bool sleep = false;
    private bool startSleep = true;

    public GameObject Plate { get { return plate; } set { value = plate; } }
    public float EveningShift { get { return eveningShift; } }
    public List<float> Convert { get { return convert; } }


    private void Awake()
    {
        nav = GetComponent<NavMeshAgent>();
    }
    void Start()
    {
        ConvertTimer();
        resource = ResourceManager.Instance;
    }


    private bool InsideBad()
    {
        if (startSleep)
            StartCoroutine(Sleep(dalayGoInside));

        if (sleep)
        {
            nav.enabled = false;
            transform.position = Vector3.MoveTowards(transform.position, badPlayer.position, speed * Time.deltaTime);
            transform.rotation = Quaternion.RotateTowards(transform.rotation, Quaternion.Euler(0, 0, 90), speedRotation * Time.deltaTime);

            if (transform.rotation == Quaternion.Euler(0, 0, 90))
            {
                startSleep = true;
                sleep = false;
                return true;
            }
        }
        return false;
    }

    private bool OutSideBad()
    {
        if (startSleep)
        {
            StartCoroutine(Sleep(dalayGoOutside));
            Audio.OnAudio?.Invoke("wakeUp");
        }

        if (sleep)
        {
            var destination = resource.MyResources[6].positionTransform.position;

            transform.position = Vector3.MoveTowards(transform.position, destination, speed * Time.deltaTime);
            transform.rotation = Quaternion.RotateTowards(transform.rotation, Quaternion.Euler(0, 0, 0), speedRotation * Time.deltaTime);

            if (transform.rotation == Quaternion.Euler(Vector3.zero))
            {
                nav.enabled = true;
                startSleep = true;
                sleep = false;
                return true;
            }
        }
        return false;

    }

    private IEnumerator Sleep(float timer)
    {
        startSleep = false;
        yield return new WaitForSeconds(timer);
        sleep = true;
    }

    private void ConvertTimer()
    {
        convert.Add(wakeUp);
        convert.Add(startTurn);
        convert.Add(eveningShift);

        for (int i = 0; i < convert.Count; i++)
        {
            convert[i] *= 60;
        }
    }
    private void OnEnable()
    {
        PlayerDelegate.onPlayer += OutSideBad;
        PlayerDelegate.onPlayerSleep += InsideBad;
    }
    private void OnDisable()
    {
        PlayerDelegate.onPlayer -= OutSideBad;
        PlayerDelegate.onPlayerSleep -= InsideBad;
    }
}
