using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Audio
{
    public delegate void AudioControl(string name);
    public static AudioControl OnAudio;
}



public class AudioManager : MonoBehaviour
{
    public Sound[] sounds;


    private static AudioManager instance;
    public static AudioManager Instance
    {
        get
        {
            return instance;
        }
        private set { instance = value; }
    }


    private void Awake()
    {
        if (instance != null)
        {
            Destroy(gameObject);
        }
        else
        {
            instance = this;
        }


        foreach (Sound sound in sounds)
        {
            sound.source = gameObject.AddComponent<AudioSource>();
            sound.source.outputAudioMixerGroup = sound.mixerGroup;
            sound.source.clip = sound.clip;
            sound.source.loop = sound.loop;
        }
    }

    public void PlayAudio(string name)
    {
        Sound s = Array.Find(sounds, sound => sound.name == name);
        if (s == null)
            return;
        s.source.Play();

    }


    private void OnEnable()
    {
        Audio.OnAudio += PlayAudio;
    }
    private void OnDisable()
    {
        Audio.OnAudio -= PlayAudio;
    }


}
