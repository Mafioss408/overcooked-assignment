using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.AI;

public class BT_Controller : MonoBehaviour
{
    // reference
    private GameManager gameManager;
    private MenuMenager menuMenager;
    private ResourceManager resourceManager;
    private TimerManager timerManager;
    private Player player;

    private NavMeshAgent agent;
    private BT_Root tree;

    // private variable
    private string currentIngredients;

    private bool set = true;
    private bool startLoop = false;
    private bool pause = false;
    private bool startPause = true;

    [Header("Variable")]
    [SerializeField] private float distanceToKitchen;
    [SerializeField] private float timeCook;
    [SerializeField] private float timeSleep;
    [SerializeField] private float timeWaiter;
    [SerializeField] private float timeFridge;

    [Header("Reference")]
    [SerializeField] private Light worldLight;
    [SerializeField] private Light badLight;
    [SerializeField] private TextMeshProUGUI openRestourant;
    [SerializeField] private TextMeshProUGUI closedRestourant;

    public enum ActionState { Idle, Working };

    ActionState state = ActionState.Idle;

    BT_Node.Status treeStatus = BT_Node.Status.Running;


    private void Awake()
    {
        agent = GetComponent<NavMeshAgent>();
        player = GetComponent<Player>();
    }

    private void Start()
    {
        gameManager = GameManager.Instance;
        menuMenager = MenuMenager.Instance;
        resourceManager = ResourceManager.Instance;
        timerManager = TimerManager.Instance;
        timerManager.NormalTimer = false;

        BT();
    }

    private void Update()
    {
        if (startLoop)
        {
            GameLoop();
        }

        if (treeStatus == BT_Node.Status.Running)
            treeStatus = tree.Process();
    }

    #region GOTO
    BT_Node.Status GoToLocation(Vector3 destination)
    {
        float distanceToTarget = Vector3.Distance(destination, transform.position);

        if (state == ActionState.Idle)
        {
            agent.SetDestination(destination);
            state = ActionState.Working;
        }
        else if (Vector3.Distance(agent.pathEndPosition, destination) >= 2)
        {

            state = ActionState.Idle;
            return BT_Node.Status.Failure;
        }

        else if (distanceToTarget < 2)
        {
            state = ActionState.Idle;
            return BT_Node.Status.Success;
        }

        return BT_Node.Status.Running;

    }
    #endregion

    #region GOTO: GoKitchen
    BT_Node.Status GoToKitchen()
    {
        BT_Node.Status s = GoToLocation(position(8));

        if (s == BT_Node.Status.Success && timerManager.Timer >= player.Convert[1])
        {
            if (set)
            {
                menuMenager.ActiveMenuFunc(true);
                set = false;
            }
            openRestourant.enabled = true;
            closedRestourant.enabled = false;
            return BT_Node.Status.Success;
        }
        return BT_Node.Status.Running;
    }
    #endregion

    #region GOTO: GoWaiter
    BT_Node.Status GoToWaiter()
    {
        BT_Node.Status s = GoToLocation(position(7));

        if (s == BT_Node.Status.Success)
        {
            if (startPause)
            {
                StartCoroutine(Pause(timeWaiter));
                Audio.OnAudio?.Invoke("Thanks");
            }

            if (pause)
            {
                player.Plate.SetActive(false);
                startPause = true;
                menuMenager.NOrder++;
                menuMenager.Order.RemoveAt(0);
                menuMenager.SetMenuUI(0);
                pause = false;
                return BT_Node.Status.Success;

            }
        }

        return BT_Node.Status.Running;
    }
    #endregion

    #region GOTO: Sleep
    BT_Node.Status BackToSleep()
    {
        BT_Node.Status s = GoToLocation(position(6));

        if (s == BT_Node.Status.Success)
        {
            badLight.enabled = false;
            set = true;
            menuMenager.NOrder = 1;
            return BT_Node.Status.Success;
        }
        else { return BT_Node.Status.Running; }
    }
    #endregion

    #region InsideBad
    BT_Node.Status SleepTime()
    {
        var tmp = PlayerDelegate.onPlayerSleep?.Invoke();

        if (tmp == true)
        {
            startLoop = true;
            worldLight.enabled = false;
            badLight.enabled = true;
            return BT_Node.Status.Success;

        }
        return BT_Node.Status.Running;
    }
    #endregion

    #region CheckIngredients
    BT_Node.Status CheckIngredients()
    {
        for (int i = 0; i < resourceManager.MyResources.Count; i++)
        {
            if (currentIngredients.Equals(resourceManager.MyResources[i].namePosition))
            {
                if (resourceManager.MyResources[i].numRisorse <= 0)
                {
                    BT_Node.Status s = GoToLocation(resourceManager.MyResources[i].positionTransform.transform.position);

                    if (s == BT_Node.Status.Success)
                    {
                        if (startPause)
                        {
                            Audio.OnAudio?.Invoke("OpenDoor");
                            StartCoroutine(Pause(timeFridge));

                        }

                        if (pause)
                        {
                            Audio.OnAudio?.Invoke("completed");
                            startPause = true;
                            pause = false;
                            resourceManager.MyResources[i].numRisorse = 5;
                            UI.onUI?.Invoke();
                            return BT_Node.Status.Success;
                        }
                    }
                }
                else
                {
                    return BT_Node.Status.Success;
                }
            }
        }
        return BT_Node.Status.Running;
    }

    #endregion

    #region ChangeIngredients
    BT_Node.Status SetCurrentIngFirst()
    {
        currentIngredients = menuMenager.Order[0].ingredient_1;
        return BT_Node.Status.Success;
    }

    BT_Node.Status SetCurrentIngSecond()
    {
        currentIngredients = menuMenager.Order[0].ingredient_2;
        return BT_Node.Status.Success;
    }

    #endregion

    #region Cooking
    BT_Node.Status Prepare()
    {
        if (startPause)
        {
            Audio.OnAudio?.Invoke("cooking");
            StartCoroutine(Pause(timeCook));
        }

        if (pause)
        {
            player.Plate.SetActive(true);

            for (int j = 0; j < resourceManager.MyResources.Count; j++)
            {
                if (menuMenager.Order[0].ingredient_1.Equals(resourceManager.MyResources[j].namePosition))
                {
                    resourceManager.MyResources[j].numRisorse--;
                }
                if (menuMenager.Order[0].ingredient_2.Equals(resourceManager.MyResources[j].namePosition))
                {
                    resourceManager.MyResources[j].numRisorse--;
                }

                UI.onUI?.Invoke();
            }

            startPause = true;
            pause = false;
            return BT_Node.Status.Success;

        }
        return BT_Node.Status.Running;
    }


    #endregion

    #region Generics: Position
    private Vector3 position(int index)
    {
        Vector3 position = resourceManager.MyResources[index].positionTransform.position;
        return position;
    }

    #endregion

    #region GoInsideBed
    BT_Node.Status IsTime()
    {
        if (timerManager.Timer >= player.Convert[0])
        {
            timerManager.NormalTimer = true;

            var tmp = PlayerDelegate.onPlayer?.Invoke();

            if (tmp == true)
            {
                worldLight.enabled = true;
                return BT_Node.Status.Success;
            }
        }
        return BT_Node.Status.Running;
    }
    #endregion

    #region WashDishes
    BT_Node.Status GoWashDishes()
    {
        if (timerManager.Timer <= player.Convert[2])
            return GoToLocation(position(9));
        else
            return BT_Node.Status.Success;

    }

    BT_Node.Status WashDishes()
    {
        if (timerManager.Timer <= player.Convert[2])
        {
            timerManager.NormalTimer = false;

        }
        else if (timerManager.Timer >= player.Convert[2])
        {
            menuMenager.ActiveMenuFunc(false);
            timerManager.NormalTimer = true;
            closedRestourant.enabled = true;
            openRestourant.enabled = false;
            return BT_Node.Status.Success;
        }

        return BT_Node.Status.Running;
    }

    #endregion

    #region GAMELOOP
    private void GameLoop()
    {
        timerManager.NormalTimer = false;

        if (startPause)
        {
            StartCoroutine(Pause(timeSleep));
        }

        if (pause)
        {
            startPause = true;
            pause = false;
            gameManager.Round++;
            BT();
            treeStatus = BT_Node.Status.Running;
            startLoop = false;
        }
    }

    #endregion

    private IEnumerator Pause(float timer)
    {
        startPause = false;
        yield return new WaitForSeconds(timer);
        pause = true;
    }

    #region BehaviourTree
    private void BT()
    {
        tree = new BT_Root();

        BT_Sequencer wakeUp = new BT_Sequencer("Sono sveglio");
        BT_Leaf goKitchen = new BT_Leaf("Vado in cucina", GoToKitchen);
        BT_Sequencer prepareOrder = new BT_Sequencer("Preparo l'ordine");
        BT_Sequencer ingredients = new BT_Sequencer("Ho tutti gli ingredienti?");
        BT_Leaf ingredient = new BT_Leaf("Controllo per rifornimento", CheckIngredients);
        BT_Leaf cooked = new BT_Leaf("Sto cucinando", Prepare);
        BT_Leaf SetIng_1 = new BT_Leaf("Controllo il primo ingrediente", SetCurrentIngFirst);
        BT_Leaf SetIng_2 = new BT_Leaf("Controllo il secondo ingrediente", SetCurrentIngSecond);
        BT_Leaf goToWaiter = new BT_Leaf("Consegno l'ordine", GoToWaiter);
        BT_Leaf backToSleep = new BT_Leaf("Torno a dormire", BackToSleep);
        BT_Leaf insideBed = new BT_Leaf("Nel letto", SleepTime);
        BT_Leaf clock = new BT_Leaf("sveglia", IsTime);
        BT_Leaf goWash = new BT_Leaf("Vai a lavare i piatti", GoWashDishes);
        BT_Leaf wash = new BT_Leaf("Lava i piatti", WashDishes);

        // primo layer
        tree.AddChild(wakeUp);

        // secondo layer
        wakeUp.AddChild(clock);
        wakeUp.AddChild(goKitchen);
        wakeUp.AddChild(prepareOrder);
        wakeUp.AddChild(goWash);
        wakeUp.AddChild(wash);
        wakeUp.AddChild(backToSleep);
        wakeUp.AddChild(insideBed);


        //terzo layer
        for (int i = 0; i < gameManager.Round; i++)
        {

            prepareOrder.AddChild(ingredients);
            prepareOrder.AddChild(goKitchen);
            prepareOrder.AddChild(cooked);
            prepareOrder.AddChild(goToWaiter);

        }

        // Quarto layer
        ingredients.AddChild(SetIng_1);
        ingredients.AddChild(ingredient);
        ingredients.AddChild(SetIng_2);
        ingredients.AddChild(ingredient);


        //stampa
        tree.PrintTree();

    }
    #endregion
}
