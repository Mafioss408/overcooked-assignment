using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResourceManager : MonoBehaviour
{
    private static ResourceManager instance;

    public static ResourceManager Instance
    {
        get => instance;

        private set => instance = value;
    }
    [Header("Resource")]
    [SerializeField] private List<MyResource> myResource = new List<MyResource>();

    private void Awake()
    {
        if (instance != null)
        {
            Destroy(gameObject);
        }
        else
        {
            instance = this;

        }

    }

    #region GET/SET
    public List<MyResource> MyResources { get { return myResource; } set { myResource = value; } }
    #endregion
}

