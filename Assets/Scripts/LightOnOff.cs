using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightOnOff : MonoBehaviour
{
    [SerializeField] private bool lightON = false;
    [SerializeField] List<Light> lights = new List<Light>();


    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            if (lightON)
            {
                foreach (Light light in lights)
                {
                    light.gameObject.SetActive(false);
                    lightON = false;
                    Audio.OnAudio?.Invoke("LightOn");
                }
            }
            else if (!lightON)
            {
                foreach (Light light in lights)
                {
                    light.gameObject.SetActive(true);
                    lightON = true;
                    Audio.OnAudio?.Invoke("LightOn");
                }
            }

        }
    }
}
