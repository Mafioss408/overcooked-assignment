using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;


public class MenuMenager : MonoBehaviour
{
    private static MenuMenager instance;

    public static MenuMenager Instance
    {
        get => instance;

        private set => instance = value;
    }


    [Header("Variable")]
    [Header("Menus")]
    [SerializeField] private List<Menu> menus = new List<Menu>();
    [SerializeField] private List<Menu> order = new List<Menu>();
    [Header("ReferenceUI")]
    [SerializeField] private TextMeshProUGUI menuForRound;
    [SerializeField] private TextMeshProUGUI nameDish;
    [SerializeField] private TextMeshProUGUI nameIngredient_1;
    [SerializeField] private TextMeshProUGUI nameIngredient_2;
    

    [SerializeField] private int nOrder;
    private bool orderComplete = false;
    private GameManager gameManager;


    // Property

    public int NOrder { get { return nOrder; } set { nOrder = value; } }
    public List<Menu> Order { get { return order; } }



    private void Awake()
    {
        if (instance != null)
        {
            Destroy(gameObject);
        }
        else
        {
            instance = this;

        }
    }

    private void Start() => gameManager = GameManager.Instance;

    private void Update() => CreateOrderQueue();

    private void CreateOrderQueue()
    {
        if (orderComplete)
        {
            for (int i = 0; i < gameManager.Round; i++)
            {
                var tmp = Random.Range(0, menus.Count);
                order.Add(menus[tmp]);
            }
            SetMenuUI(0);
            orderComplete = false;
        }

    }

    public void SetMenuUI(int index)
    {
        if (order.Count == 0) return;
        nameDish.text = order[index].nameDish;
        nameIngredient_1.text = order[index].ingredient_1;
        nameIngredient_2.text = order[index].ingredient_2;
        menuForRound.text = nOrder + "/" + gameManager.Round;

    }

    public void ActiveMenuFunc(bool tmp)
    {
        menuForRound.gameObject.SetActive(tmp);
        nameDish.gameObject.SetActive(tmp);
        nameIngredient_1.gameObject.SetActive(tmp);
        nameIngredient_2.gameObject.SetActive(tmp);
        orderComplete = tmp;
    }

}
